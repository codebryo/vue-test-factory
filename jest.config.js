module.exports = {
  moduleFileExtensions: [
    'js',
    'json'
  ],
  cache: false,
  transform: {
    "^.+\\.[t|j]s?$": "babel-jest"
  },
  transformIgnorePatterns: [
    '/node_modules/'
  ],
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/dist/$1'
  },
  testMatch: [
    '**/specs/**/*.spec.(js|ts)'
  ],
  testURL: 'http://localhost/',
  watchPlugins: [
    'jest-watch-typeahead/filename',
    'jest-watch-typeahead/testname'
  ]
}
