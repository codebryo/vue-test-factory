import valuesToFunctions from './support/values-to-functions'
import { createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'

export function createFactory(fn, config = {}) {
  if (typeof fn !== 'function') {
    throw new Error('`createFactory` expects a function as first parameter')
  }

  const defaultConfig = {
    stubs: {},
    props: {},
    data: {}
  }

  // Allow the factory function to accept custom options
  const factory = function(options) {
    const params = Object.assign({}, defaultConfig, config, options)
    return fn(params)
  }

  return factory
}

export function storeFactory(storeConfig = {}, localVue = undefined) {
  const mocked = storeConfig.mock === true ? true : false
  if (!localVue) localVue = createLocalVue()
  localVue.use(Vuex)

  const config = !mocked ? storeConfig : {
    state() {
      return {
        ...storeConfig.state
      }
    },
    getters: {
      ...valuesToFunctions(storeConfig.getters)
    },
    mutations: {
      ...storeConfig.mutations
    },
    actions: {
      ...storeConfig.actions
    }
  }

  const store = new Vuex.Store(config)

  return {
    localVue,
    store
  }
}
