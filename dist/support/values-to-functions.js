export default function valuesToFunctions(object = {}) {
  return Object.entries(object).reduce((result, [key, value]) => {
    if (typeof value === 'function') result[key] = value
    else result[key] = () => value
    return result
  }, {})
}
