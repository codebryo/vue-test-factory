import { storeFactory } from '@/index'

const exampleStore = {
  state() {
    return {
      version: 1
    }
  },

  mutations: {
    updateVersion(state) {
      state.version++
    }
  },

  getters: {
    version: s => s.version
  }
}

describe('storeFactory', () => {
  test('will return a proper vuex store', () => {
    const { store } = storeFactory(exampleStore)
    expect(store.state.version).toBe(1)
    store.commit('updateVersion')
    expect(store.state.version).toBe(2)
    expect(store.getters.version).toBe(2)
  })

  test('can easily mock a store', () => {
    const fakeStore = {
      mock: true,
      getters: {
        names: ['Bruce Wayne', 'Mr Deadpool']
      }
    }
    const { store } = storeFactory(fakeStore)
    expect(store.getters.names).toEqual(fakeStore.getters.names)

  })
})
