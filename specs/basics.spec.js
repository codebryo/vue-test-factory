import { createFactory } from '@/index'

describe('#createFactory', () => {
  test('throws Error if nothing is passed', () => {
    expect(() => createFactory()).toThrowError('`createFactory` expects a function as first parameter')
  })

  test('returns a new function with Vue default parameters', () => {
    const testFactory = (options) => {
      return options
    }

    const factory = createFactory(testFactory)

    expect(typeof factory).toBe('function')
    expect(factory()).toMatchSnapshot()

  })

  test('can invoke factory with custom data', () => {
    const factory = createFactory(({ props }) => {
      return props
    })

    expect(factory({ props: { a: 1, b: 'foo' }})).toEqual({ a: 1, b: 'foo'})
  })
})
