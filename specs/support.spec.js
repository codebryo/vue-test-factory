import valuesToFunctions from '@/support/values-to-functions'

describe('valuesToFunctions', () => {
  test('turns key value object into functions', () => {
    const data = {
      a: 1,
      thing: 'A long answer',
      noChange: (str) => `Test: ${str}`
    }

    const transformed = valuesToFunctions(data)

    expect(transformed.a()).toBe(1)
    expect(transformed.thing()).toBe('A long answer')
    expect(transformed.noChange('Passed')).toBe('Test: Passed')
  })
})
